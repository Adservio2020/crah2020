package com.adservio.crah.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/absence")
public class AbsenceController {

	/**
     * Ajoute un nouveau absence
     * @param 
     * @return
     */
    @PostMapping("/addAbsence")
    public ResponseEntity<Absence> addAbsence(@RequestBody Absence) {
       
        return new ResponseEntity<Absence>(HttpStatus.NOT_MODIFIED);
    }
    
    /**
     * Renvoie tous les types d'absence
     * @param 
     * @return
     */
    @GetMapping("/allTypeAbsence")
    public ResponseEntity<Absence> allTypeAbsence(@RequestBody Absence) {
       
        return new ResponseEntity<Absence>(HttpStatus.NOT_MODIFIED);
    }
}

package com.adservio.crah.rest;

import java.net.URI;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adservio.crah.criteria.UserCriteria;
import com.adservio.crah.dto.UserCreationRequest;
import com.adservio.crah.entities.User;
import com.adservio.crah.services.UserService;
import com.adservio.crah.validator.UserCreationRequestValidator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/users")
@Api(value="Crah-profile")
public class UserController {

	private final Logger log = LoggerFactory.getLogger(UserController.class);
		
	@Autowired
	private UserService userService;
	
	@Autowired
	private Environment environment;
	
	@InitBinder()
	protected void initBinder(WebDataBinder binder) {
		if(binder.getTarget() instanceof UserCreationRequest)
			binder.setValidator(new UserCreationRequestValidator());
	}
	
	@PostMapping(produces = "application/json")
	@ApiOperation(value = "Créé un nouvel utilisateur", response = User.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 201, message = "Création effectuée avec succès"),
	        @ApiResponse(code = 400, message = "Cet utilisateur existe déjà"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public ResponseEntity<User> createUser(@Valid @RequestBody UserCreationRequest userCreationRequest) throws Exception{
		log.info("REST request to save a user : {}", userCreationRequest);
		User result = userService.createUser(userCreationRequest);
		return ResponseEntity.created(new URI("/api/users/" + result.getId()))
                .body(result);
	}
	
	@PutMapping(value = "/{id}", produces = "application/json")
	@ApiOperation(value = "Modifie un utilisateur existant", response = User.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Utilisateur modifié avec succès"),
	        @ApiResponse(code = 400, message = "Un tel utilisateur existe déjà"),
	        @ApiResponse(code = 404, message = "Utilisateur non trouvé"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public ResponseEntity<User> updatePeriod(@PathVariable Long id, 
							@Valid @RequestBody UserCreationRequest userCreationRequest) throws Exception{
		log.info("REST request to update a user with ID: {} and parameters {}", id, userCreationRequest);
		User result = userService.updateUser(id, userCreationRequest);
		return ResponseEntity.ok()
                .body(result);
	}
	
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "Retourne la liste des utilisateurs selon des critères pages par pages")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "retourne la liste des utilisateurs selon des critères pages par pages"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public ResponseEntity<Page<User>> getAllUserByPage(UserCriteria criteria, int page){
		log.debug("REST request to get users by criteria: {}", criteria);
		Page<User> result = userService.findAllUserByCriteria(criteria,
				new PageRequest(page, Integer.parseInt(environment.getProperty("pages.number"))));
		return ResponseEntity.ok()
                .body(result);
	}
	
	@PutMapping(value = "/{id}/reactiver", produces = "application/json")
	@ApiOperation(value = "Reactive un utilisateur")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Utilisateur réactivé avec succès"),
	        @ApiResponse(code = 404, message = "Utilisateur non trouvé"),
	        @ApiResponse(code = 409, message = "Utilisateur déjà actif"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public void reactivateUser(@PathVariable Long id) throws Exception{
		log.debug("REST request to reactivate the user id: {}", id);
		userService.activateUser(id);
	}
	
	@PutMapping(value = "/{id}/desactiver", produces = "application/json")
	@ApiOperation(value = "Desactive un utilisateur")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Utilisateur désactivé avec succès"),
	        @ApiResponse(code = 404, message = "Utilisateur non trouvé"),
	        @ApiResponse(code = 409, message = "Utilisateur déjà actif"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public void desactivateUser(@PathVariable Long id) throws Exception{
		log.debug("REST request to desactivate the user id: {}", id);
		userService.desactivateUser(id);
	}
	
}

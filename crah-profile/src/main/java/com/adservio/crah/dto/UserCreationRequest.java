package com.adservio.crah.dto;

public class UserCreationRequest {

	private String nom;
	
	private String prenom;
	
	private String matricule;
	
	private String email;
	
	private String adresse;
	
	private String telephone;
	
	private String motDePasse;
	
	private String roleId;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "UserCreationRequest [nom=" + nom + ", prenom=" + prenom + ", matricule=" + matricule + ", email="
				+ email + ", adresse=" + adresse + ", telephone=" + telephone + ", motDePasse=" + motDePasse
				+ ", roleId=" + roleId + "]";
	}
	
	
}

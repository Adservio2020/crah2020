package com.adservio.crah.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "roles")
public class Role {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private String id;
	
	@Column(name = "role_name")
	private String roleName;
	
	@JsonIgnore
	@OneToMany(
	        mappedBy = "role",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
    )
    private List<User> comments = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<User> getComments() {
		return comments;
	}

	public void setComments(List<User> comments) {
		this.comments = comments;
	}
	
	
}

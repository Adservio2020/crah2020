package com.adservio.crah;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@RefreshScope
@EnableEurekaClient
public class CrahProfileApplication {
	
private static final Logger log = LoggerFactory.getLogger(CrahProfileApplication.class); 
 
	public static void main(String[] args) {
		SpringApplication.run(CrahProfileApplication.class, args);
	}

}

package com.adservio.crah.services;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adservio.crah.criteria.UserCriteria;
import com.adservio.crah.criteria.UserSpecification;
import com.adservio.crah.dto.UserCreationRequest;
import com.adservio.crah.entities.Role;
import com.adservio.crah.entities.User;
import com.adservio.crah.enums.UserStatus;
import com.adservio.crah.exceptions.EntityAlreadyExistException;
import com.adservio.crah.exceptions.EntityNotFoundException;
import com.adservio.crah.exceptions.IncorrectStateException;
import com.adservio.crah.repositories.RoleRepository;
import com.adservio.crah.repositories.UserRepository;

@Service
@Transactional
public class UserService {

	private final Logger log = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private CryptoService cryptoService;
	
	public User createUser(UserCreationRequest userCreationRequest) throws EntityAlreadyExistException, EntityNotFoundException{
		
		log.info("Contrôle sur l'utilisateur qu'on veut enregistrer");
		Optional<Role> role = roleRepository
				.findById(userCreationRequest.getRoleId());
		if(!role.isPresent())
			throw new EntityNotFoundException("Role non existant");
		
		Optional<User> userWithEmail =userRepository
				.findByEmail(userCreationRequest.getEmail());
		if(userWithEmail.isPresent())
			throw new EntityAlreadyExistException("Un utilisateur avec cet email existe déjà");
		
		Optional<User> userWithMatricule =userRepository
				.findByMatricule(userCreationRequest.getMatricule());
		if(userWithMatricule.isPresent())
			throw new EntityAlreadyExistException("Un utilisateur avec ce matricule existe déjà");
		
		log.info("Enregistrement de l'utilisateur");
		User userToSave = _convertUserRequestToUser(userCreationRequest);
		userToSave.setRole(role.get());
		userToSave.setActive(UserStatus.ACTIVE.getStatusName());
		
		return userRepository.save(userToSave);
		
	}
	
	public User updateUser(Long id, UserCreationRequest userCreationRequest) throws EntityAlreadyExistException, EntityNotFoundException{
		
		log.info("Modification de l'utilisateur");
		Optional<Role> role = roleRepository
				.findById(userCreationRequest.getRoleId());
		if(!role.isPresent())
			throw new EntityNotFoundException("Role non existant");
		
		Optional<User> userWithId = userRepository.findById(id);
		if(!userWithId.isPresent())
			throw new EntityNotFoundException("Utilisateur non existant");
		
		Optional<User> userWithEmail =userRepository
				.findByEmail(userCreationRequest.getEmail());
		if(userWithEmail.isPresent() && !userWithEmail.get().getId().equals(id))
			throw new EntityAlreadyExistException("Un autre utilisateur avec cet email existe déjà");
		
		Optional<User> userWithMatricule =userRepository
				.findByMatricule(userCreationRequest.getMatricule());
		if(userWithMatricule.isPresent() && !userWithMatricule.get().getId().equals(id))
			throw new EntityAlreadyExistException("Un autre utilisateur avec ce matricule existe déjà");
		
		log.info("Enregistrement de l'utilisateur");
		User userToSave = _convertUserRequestToUser(userCreationRequest);
		userToSave.setRole(role.get());
		
		return userRepository.saveAndFlush(userToSave);
	}
	
	public void desactivateUser(Long userId) throws EntityNotFoundException, IncorrectStateException{
	
		log.info("Desactivation de l'utilisateur avec ID: {}", userId);
		Optional<User> userWithId = userRepository.findById(userId);
		if(!userWithId.isPresent())
			throw new EntityNotFoundException("Utilisateur non existant");
		
		if(userWithId.get().getActive().equals(UserStatus.INACTIVE.getStatusName()))
			throw new IncorrectStateException("Utilisateur déjà désactivé");
		
		userWithId.get().setActive(UserStatus.INACTIVE.getStatusName());
		userRepository.saveAndFlush(userWithId.get());
	}
	
	public void activateUser(Long userId) throws EntityNotFoundException, IncorrectStateException{
		
		log.info("Activation de l'utilisateur avec ID: {}", userId);
		Optional<User> userWithId = userRepository.findById(userId);
		if(!userWithId.isPresent())
			throw new EntityNotFoundException("Utilisateur non existant");
		
		if(userWithId.get().getActive().equals(UserStatus.ACTIVE.getStatusName()))
			throw new IncorrectStateException("Utilisateur déjà activé");
		
		userWithId.get().setActive(UserStatus.ACTIVE.getStatusName());
		userRepository.saveAndFlush(userWithId.get());
	}
	
	
	public Page<User> findAllUserByCriteria(UserCriteria userCriteria, Pageable pageable) {
		
		log.info("Recherche de tous les utilisateurs avec les critères : {}", userCriteria);
		Specification<User> userSpecification = _createSpecification(userCriteria);
		return userRepository.findAll(userSpecification, pageable);
	}
	
	private Specification<User> _createSpecification(UserCriteria criteria) {
        return new UserSpecification(criteria);
	}
	
	private User _convertUserRequestToUser(UserCreationRequest userCreationRequest) {
		User user = new User();
		user.setAdresse(userCreationRequest.getAdresse());
		user.setEmail(userCreationRequest.getEmail());
		user.setMatricule(userCreationRequest.getMatricule());
		user.setMotDepasse(cryptoService.encryptString(userCreationRequest.getMotDePasse()));
		user.setNom(userCreationRequest.getNom());
		user.setPrenom(userCreationRequest.getPrenom());
		user.setTelephone(userCreationRequest.getTelephone());		
		
		return user;
	}
	
}

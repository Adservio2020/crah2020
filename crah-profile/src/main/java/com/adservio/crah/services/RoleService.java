package com.adservio.crah.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adservio.crah.entities.Role;
import com.adservio.crah.repositories.RoleRepository;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;
	
	public List<Role> getListRole() {
		return roleRepository.findAll();
	}
}

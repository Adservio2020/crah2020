package com.adservio.crah.enums;

public enum UserStatus {

	ACTIVE("ACTIVE"),
	INACTIVE("INACTIVE");
	
	private String statusName;

	public String getStatusName() {
		return statusName;
	}

	private UserStatus(String statusName) {
		this.statusName = statusName;
	}
	
}

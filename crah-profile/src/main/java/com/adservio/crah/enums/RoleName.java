package com.adservio.crah.enums;

public enum RoleName {

	ADMIN("ADMIN"),
	USER("USER");
	
	private String roleName;

	private RoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleName() {
		return roleName;
	}
	
}

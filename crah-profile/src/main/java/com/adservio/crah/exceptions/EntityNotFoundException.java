package com.adservio.crah.exceptions;

public class EntityNotFoundException extends CrahProfileException {

	private static final long serialVersionUID = 1L;

	public EntityNotFoundException() {
		super();
	}

	public EntityNotFoundException(String errorMessage) {
		super(errorMessage);
	}
	
}

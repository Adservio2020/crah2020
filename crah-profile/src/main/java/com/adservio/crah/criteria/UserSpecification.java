package com.adservio.crah.criteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.adservio.crah.entities.User;

/**
 * This class is used for period filter purpose
 * @author gokengtatsir
 *
 */
public class UserSpecification implements Specification<User>{

	private UserCriteria filter;
	
	public UserSpecification(UserCriteria filter) {
		super();
		this.filter=filter;
	}
	
	@Override
	public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		
		Predicate predicate= criteriaBuilder.conjunction();
		if(filter.getActive()!=null)
			predicate.getExpressions().add(criteriaBuilder.equal(root.get("active"), filter.getActive()));
		if(filter.getAdresse()!=null)
			predicate.getExpressions().add(criteriaBuilder.like(root.get("adresse"), "%"+filter.getAdresse()+"%"));
		if(filter.getEmail()!=null)
			predicate.getExpressions().add(criteriaBuilder.like(root.get("email"), "%"+filter.getEmail()+"%"));
		if(filter.getMatricule()!=null)
			predicate.getExpressions().add(criteriaBuilder.like(root.get("matricule"), "%"+filter.getMatricule()+"%"));
		if(filter.getNom()!=null)
			predicate.getExpressions().add(criteriaBuilder.like(root.get("nom"), "%"+filter.getNom()+"%"));
		if(filter.getPrenom()!=null)
			predicate.getExpressions().add(criteriaBuilder.like(root.get("prenom"), "%"+filter.getPrenom()+"%"));
		if(filter.getTelephone()!=null)
			predicate.getExpressions().add(criteriaBuilder.like(root.get("telephone"), "%"+filter.getTelephone()+"%"));
		
		return criteriaBuilder.and(predicate);
	}

}

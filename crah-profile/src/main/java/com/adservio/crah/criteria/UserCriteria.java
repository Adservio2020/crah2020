package com.adservio.crah.criteria;

import java.io.Serializable;

public class UserCriteria implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String nom;
		
	private String prenom;
	
	private String matricule;
	
	private String email;
	
	private String active;
	
	private String adresse;
	
	private String telephone;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "UserCriteria [nom=" + nom + ", prenom=" + prenom + ", matricule=" + matricule + ", email=" + email
				+ ", active=" + active + ", adresse=" + adresse + ", telephone=" + telephone + "]";
	}
}

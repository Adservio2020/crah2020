package com.adservio.crah.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adservio.crah.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, String>{

	Optional<Role> findById(String id);
}

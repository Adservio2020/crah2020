package com.adservio.crah.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.adservio.crah.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>,  JpaSpecificationExecutor<User>{

	Optional<User> findByEmail(String email);
	
	Optional<User>  findByMatricule(String matricule);
	
	Optional<User> findById(Long id);
	
	
}

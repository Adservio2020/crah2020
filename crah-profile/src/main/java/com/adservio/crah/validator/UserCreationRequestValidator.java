package com.adservio.crah.validator;

import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.adservio.crah.dto.UserCreationRequest;

public class UserCreationRequestValidator implements Validator {

	private final static Pattern EMAIL_PATTERN = Pattern.compile(".+@.+\\.[a-z]+");
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return  UserCreationRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		UserCreationRequest userCreationRequest = (UserCreationRequest) target;
		
		if(userCreationRequest.getEmail()==null || userCreationRequest.getEmail().trim().isEmpty())
			errors.rejectValue("email","", "Email requis");
		else {
			if(!_isEmail(userCreationRequest.getEmail()))
				errors.rejectValue("email","", "Email invalide");
		}
		
		if(userCreationRequest.getMatricule()==null || userCreationRequest.getMatricule().trim().isEmpty())
			errors.rejectValue("matricule","", "Matricule requis");
		
		if(userCreationRequest.getAdresse()==null || userCreationRequest.getAdresse().trim().isEmpty())
			errors.rejectValue("adresse","", "Adresse requis");
		
		if(userCreationRequest.getMotDePasse()==null || userCreationRequest.getMotDePasse().trim().isEmpty())
			errors.rejectValue("motDePasse","", "Mot de passe requis");
		
		if(userCreationRequest.getNom()==null || userCreationRequest.getNom().trim().isEmpty())
			errors.rejectValue("nom","", "Nom requis");
		
		if(userCreationRequest.getTelephone()==null || userCreationRequest.getTelephone().trim().isEmpty())
			errors.rejectValue("nom","", "Telephone requis");
		
		if(userCreationRequest.getRoleId()==null || userCreationRequest.getRoleId().trim().isEmpty())
			errors.rejectValue("role","", "Role requis");
		
	}
	
	private boolean _isEmail(String value) {
		return EMAIL_PATTERN.matcher(value).matches();
    }

}

package com.adservio.crah.criteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.adservio.crah.entities.Project;

/**
 * This class is used for period filter purpose
 * @author gokengtatsir
 *
 */
public class ProjectSpecification implements Specification<Project>{

	private ProjectCriteria filter;
	
	public ProjectSpecification(ProjectCriteria filter) {
		super();
		this.filter=filter;
	}
	
	@Override
	public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		
		Predicate predicate= criteriaBuilder.conjunction();
		if(filter.getProjectName()!=null)
			predicate.getExpressions().add(criteriaBuilder.like(root.get("projectName"), "%"+filter.getProjectName()+"%"));
		
		return criteriaBuilder.and(predicate);
	}

}

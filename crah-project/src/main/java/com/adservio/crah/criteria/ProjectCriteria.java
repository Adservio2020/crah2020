package com.adservio.crah.criteria;

import java.io.Serializable;

public class ProjectCriteria implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String projectName;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}

package com.adservio.crah.services;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.adservio.crah.criteria.ProjectCriteria;
import com.adservio.crah.criteria.ProjectSpecification;
import com.adservio.crah.dto.CreateProjectRequest;
import com.adservio.crah.entities.Project;
import com.adservio.crah.exceptions.EntityAlreadyExistException;
import com.adservio.crah.exceptions.EntityNotFoundException;
import com.adservio.crah.repositories.ProjectRepository;

@Service
public class ProjectService{

	private final Logger log = LoggerFactory.getLogger(ProjectService.class);
	
	@Autowired
	private ProjectRepository projectRepository;
	
	public Project createProject(CreateProjectRequest createProjectRequest) throws Exception {
		
		log.info("Creation du projet: {}",createProjectRequest);
		Optional<Project> projectToFind = projectRepository
				.findByProjectName(createProjectRequest.getProjectName());
		if(projectToFind.isPresent())
			throw new EntityAlreadyExistException("Un projet de ce nom existe déjà");
		Project projet = new Project();
		projet.setProjectName(createProjectRequest.getProjectName());
		
		return projectRepository.save(projet);
	}

	public Project updateProject(Long id, CreateProjectRequest createProjectRequest) throws Exception {
	
		log.info("Modification du projet: {}",createProjectRequest);
		Optional<Project> projectToFindById = projectRepository
				.findById(id);	
		if(!projectToFindById.isPresent())
			throw new EntityNotFoundException("Projet Inexistant");
		projectToFindById.get().setProjectName(createProjectRequest.getProjectName());
		
		//Recherche si un projet existe déjà avec le même nom
		Optional<Project> projectToFindByName = projectRepository
				.findByProjectName(createProjectRequest.getProjectName());
		if(projectToFindByName.isPresent() && !projectToFindByName.get().getId().equals(id))
			throw new EntityAlreadyExistException("Un autre projet de ce nom existe déjà");
			
		return projectRepository.saveAndFlush(projectToFindById.get());
	}

	public Page<Project> pageOfProject(Pageable pageable, ProjectCriteria projectCriteria) {
		
		log.info("Recherche de tous les projets avec les critères : {}", projectCriteria);
		Specification<Project> projectSpecification = _createSpecification(projectCriteria);
		return projectRepository.findAll(projectSpecification, pageable);
	}

	public void deleteProject(Long id) throws Exception {
		Optional<Project> projectToFind = projectRepository.findById(id);
		if(!projectToFind.isPresent())
			throw new EntityNotFoundException("Projet non existant");
		projectRepository.delete(id);		
	}

	public List<Project> listOfProject() {		
		return projectRepository.findAll();
	}
	
	private Specification<Project> _createSpecification(ProjectCriteria criteria) {
        return new ProjectSpecification(criteria);
	}

}

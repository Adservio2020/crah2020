package com.adservio.crah.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.adservio.crah.dto.CreateProjectRequest;

public class ProjectCreationRequestValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		return  CreateProjectRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		CreateProjectRequest projectCreationRequest = (CreateProjectRequest) target;
		
		if(projectCreationRequest.getProjectName()==null || projectCreationRequest.getProjectName().trim().isEmpty())
			errors.rejectValue("projectName","", "Nom du projet requis");
	}
	
}

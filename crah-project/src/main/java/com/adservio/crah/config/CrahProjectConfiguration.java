package com.adservio.crah.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class CrahProjectConfiguration {

	@Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.adservio.crah.rest"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaData());
    }
	
	private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("Crah project REST API")
                .description("\"API pour toutes les fonctions relatives à la gestion des projets\"")
                .version("1.0.0")
                .license("Copyright ADSERVIO ")
                .contact(new Contact("Adservio team", "", "contact@adservio.com"))
                .build();
    }
}

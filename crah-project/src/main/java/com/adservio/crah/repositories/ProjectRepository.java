package com.adservio.crah.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.adservio.crah.entities.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long>, JpaSpecificationExecutor<Project>{

	Optional<Project>  findByProjectName(String projectName);
	
	Optional<Project> findById(Long id);	
}

package com.adservio.crah;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@RefreshScope
@EnableEurekaClient
public class CrahProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrahProjectApplication.class, args);
	}

}



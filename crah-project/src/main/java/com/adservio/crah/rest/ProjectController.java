package com.adservio.crah.rest;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adservio.crah.criteria.ProjectCriteria;
import com.adservio.crah.dto.CreateProjectRequest;
import com.adservio.crah.entities.Project;
import com.adservio.crah.services.ProjectService;
import com.adservio.crah.validator.ProjectCreationRequestValidator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/projects")
@Api(value="Crah-project")
public class ProjectController {

	private final Logger log = LoggerFactory.getLogger(ProjectController.class);
		
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private Environment environment;
	
	@InitBinder()
	protected void initBinder(WebDataBinder binder) {
		if(binder.getTarget() instanceof CreateProjectRequest)
			binder.setValidator(new ProjectCreationRequestValidator());
	}
	
	@PostMapping(produces = "application/json")
	@ApiOperation(value = "Créé un nouveau projet", response = Project.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 201, message = "Création effectuée avec succès"),
	        @ApiResponse(code = 400, message = "Ce projet existe déjà"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public ResponseEntity<Project> createUser(@Valid @RequestBody CreateProjectRequest createProjectRequest) throws Exception{
		log.info("REST request to save a project : {}", createProjectRequest);
		Project result = projectService.createProject(createProjectRequest);
		return ResponseEntity.created(new URI("/api/projects/" + result.getId()))
                .body(result);
	}
	
	@PutMapping(value = "/{id}", produces = "application/json")
	@ApiOperation(value = "Modifie un projet existant", response = Project.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Utilisateur modifié avec succès"),
	        @ApiResponse(code = 400, message = "Un tel utilisateur existe déjà"),
	        @ApiResponse(code = 404, message = "Utilisateur non trouvé"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public ResponseEntity<Project> updatePeriod(@PathVariable Long id, 
							@Valid @RequestBody CreateProjectRequest createProjectRequest) throws Exception{
		log.info("REST request to update a project with ID: {} and parameters {}", id, createProjectRequest);
		Project result = projectService.updateProject(id, createProjectRequest);
		return ResponseEntity.ok()
                .body(result);
	}
	
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "Retourne la liste des projets selon des critères pages par pages")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "retourne la liste des projets selon des critères pages par pages"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public ResponseEntity<Page<Project>> getAllProjectByPage(ProjectCriteria criteria, int page){
		
		log.debug("REST request to get users by criteria: {}", criteria);
		Page<Project> result = projectService
				.pageOfProject(new PageRequest(page, Integer.parseInt(environment.getProperty("pages.number"))),criteria);
		return ResponseEntity.ok()
                .body(result);
	}
	
	@GetMapping(value = "/list",produces = "application/json")
	@ApiOperation(value = "Retourne la liste de tous les projets")
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Retourne la liste de tous les projets"),
	        @ApiResponse(code = 500, message = "Internal server error")
	}
	)
	public ResponseEntity<List<Project>> getAllProject(){
		
		log.debug("REST request to get all the projects");
		List<Project> result = projectService.listOfProject();
		return ResponseEntity.ok()
                .body(result);
	}	
}

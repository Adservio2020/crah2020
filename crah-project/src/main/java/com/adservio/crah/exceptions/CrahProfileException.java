package com.adservio.crah.exceptions;

public class CrahProfileException extends Exception{

	private static final long serialVersionUID = 1L;

	public CrahProfileException() {
		super();
	}

	public CrahProfileException(String errorMessage) {
		super(errorMessage);
	}
}

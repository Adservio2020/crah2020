package com.adservio.crah.exceptions;

public class IncorrectStateException extends CrahProfileException {

	private static final long serialVersionUID = 1L;

	public IncorrectStateException() {
		super();
	}

	public IncorrectStateException(String errorMessage) {
		super(errorMessage);
	}
	
}

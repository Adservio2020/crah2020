package com.adservio.crah.exceptions;

public class EntityAlreadyExistException extends CrahProfileException {

	private static final long serialVersionUID = 1L;

	public EntityAlreadyExistException() {
		super();
	}

	public EntityAlreadyExistException(String errorMessage) {
		super(errorMessage);
	}
	
}
